#pragma once
typedef DWORD COLOREF;

// 枚举方向
enum direction { up, down, left_t, right_t };
typedef struct XyColour
{
	int x;
	int y;
	DWORD colour;		// 颜色
};

// 玩家
typedef struct User
{
	int shengming;
	int defeng;
};

// 用户
void UserItIn(User* user_t)
{
	user_t->shengming = 1;
	user_t->defeng = 0;
}

// 延时函数
void Delay(unsigned int nDelay)
{
	unsigned int i, j, k = 0;
	for (size_t i = 0; i < nDelay; i++)
		for (size_t j = 0; j < 6114; j++)
			k++;
}

// 分数
void MarkDow(User user_t)
{
	// 绘制用户界面
	setfillcolor(BROWN);
	fillrectangle(0, 0, 200, 800);
	wchar_t w[] = L"用户名:弱弱";
	settextstyle(25, 10, _T("w"));
	outtextxy(10, 200, w);

	// 输出得分
	settextcolor(WHITE);
	wchar_t  defeng[100];
	swprintf_s(defeng, _T("得分：%d"), user_t.defeng);		// 高版本 VC 推荐使用 _stprintf_s 函数
	outtextxy(10, 60, defeng);
}



