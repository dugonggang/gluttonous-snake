#pragma once
#include"sNake.h"

// 食物
typedef struct Food
{
	XyColour foodXYC;
	int survive;		// 生存标示
	int experience;		// 经验
	int vernierLine;	// 游标列
	int vernierRow;		// 游标行
};

// 初始化食物数据
void InitializeFood(Food* foodHeade[3], int x)
{
	// 设置游标
	for (size_t i = 0; i < 3; i++)
	{
		for (size_t j = 0; j < 3; j++)
		{
			do
			{
				// 坐标 x y
				foodHeade[i][j].foodXYC.x = 100;
				foodHeade[i][j].foodXYC.y = rand() % 800;
			} while (foodHeade[i][j].foodXYC.x <= 200 && foodHeade[i][j].foodXYC.y > 0);
			// 颜色
			foodHeade[i][j].foodXYC.colour = BGR(RGB(rand() & 0xFF, rand() & 0xFF, rand() & 0xFF));
			// 生存标示
			foodHeade[i][j].survive = 1;
			// 经验
			foodHeade[i][j].experience = 1;
			// 游标
			foodHeade[i][j].vernierRow = i;
			foodHeade[i][j].vernierLine = j + 1;
		}
	}
	// 首结点
	foodHeade[0][0].experience = NULL;
	foodHeade[0][0].foodXYC.colour = NULL;
	foodHeade[0][0].foodXYC.x = NULL;
	foodHeade[0][0].foodXYC.y = NULL;
	foodHeade[0][0].survive = 0;
	foodHeade[0][0].vernierRow = 2;
	foodHeade[0][0].vernierLine = 2;
	
	// 尾结点
	foodHeade[9][9].experience = NULL;
	foodHeade[9][9].foodXYC.colour = NULL;
	foodHeade[9][9].foodXYC.x = NULL;
	foodHeade[9][9].foodXYC.y = NULL;
	foodHeade[9][9].survive = 0;
	foodHeade[9][9].vernierLine = 0;
	foodHeade[9][9].vernierRow = 1;
}

// 画食物
void DrawFood(Food* food_t[10])
{
	for (size_t i = 0; i < 10; i++)
	{
		for (size_t j = 0; j < 10; j++)
		{
			if (food_t[i][j].survive == 1)
			{
				setfillcolor(food_t[i][j].foodXYC.colour);
				fillrectangle((food_t[i][j].foodXYC.x), (food_t[i][j].foodXYC.y), (food_t[i][j].foodXYC.x) + 50, (food_t[i][j].foodXYC.y) + 50);
			}
			
		}
	}

}
