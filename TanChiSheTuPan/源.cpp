#include <graphics.h>		// 引用图形库头文件
#include <conio.h>
#include <stdlib.h>
#include<iostream>
#include<Windows.h>
#include<mmsystem.h>
#pragma comment(lib,"winmm.lib")
using namespace std;
#include <time.h> //用到了time函数 
#include"sNake.h"
#include"Food.h"

IMAGE beijing;
IMAGE Snaketp;
// 初始化
void GameInit()
{
	initgraph(1500, 800, SHOWCONSOLE);
	//cleardevice();
	// 背景颜色
	//loadimage(&beijing, L"./biejingtp.png",2700,1650);
	loadimage(&beijing, L"./SnakeBJ00.png");
	
}


// 游戏界面初始化
void GameInit_2(XyColour backeGround_t)
{
	// 画背景图片
	putimage(0, 0,1500,800, &beijing, backeGround_t.x, backeGround_t.y);
}


// 获取用户操作
void GetUser(Snake* snake_t)
{
	if (GetAsyncKeyState(0x57) & 0x8000) //HS上
	{
		if (snake_t->direction != down)
			snake_t->direction = up;
		printf("上");
		return;
	}
	if (GetAsyncKeyState(0x53) & 0x8000)//下
	{
		if (snake_t->direction != up)
			snake_t->direction = down;
		printf("下");
		return;
	}
	if (GetAsyncKeyState(0x41) & 0x8000)//左
	{
		if (snake_t->direction != right_t)
			snake_t->direction = left_t;
		printf("左");
		return;
	}
	if (GetAsyncKeyState(0x44) & 0x8000)//右
	{
		if (snake_t->direction != left_t)
			snake_t->direction = right_t;
		printf("右");
		return;
	}
}

// 死亡游戏结束
void SnakeDie(Snake* snake_t, User* user_t)
{
	HWND hwnd = NULL;
	if (snake_t->snakeXYC.x > 1450 || snake_t->snakeXYC.x < 200 || snake_t->snakeXYC.y >= 800 || snake_t->snakeXYC.y <= -50)
	{
		PlaySound(TEXT("gameOuver"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
		user_t->shengming = 0;
		MessageBox(hwnd, L"撞墙死亡", L"gemeover", MB_OK);
	}
}

int main()
{
	Snake* snakeheade;
	Food food[10][10];
	User user;
	Food *foodHeade;
	XyColour backeGround = {0, 0, NULL};// 背景的坐标
	// 加载数据
	GameInit();
	// 初始化用户
	UserItIn(&user);
	// 蛇的头结点和尾结点
	SnakeHead(&snakeheade);
	// 初始化成蛇的数据
	InitializeSnake(snakeheade, 5);
	
	// 初始化食物数据
	InitializeFood(&foodHeade,3);
	 //音乐播放
	PlaySound(TEXT("Snake (1)"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
	while (user.shengming != 0)
	{
		BeginBatchDraw();
		// 刷新路径
		cleardevice();
		// 游戏界面初始化
		GameInit_2(backeGround);
		// 显示分数
		MarkDow(user);
		Output(snakeheade);
		// 蛇行走
		SnakeCraw(snakeheade, &backeGround);
		
		// 画蛇
		DrawSnake(snakeheade);
		
		// 画食物
		DrawFood(&foodHeade);
		// 吃食物
		//EatFood(snakeheade, foodHeade, &user);
		// 获取用户操作
		GetUser(snakeheade);
		if (user.defeng != 0 && user.defeng % 30 == 0)
		{
			// 初始化食物数据
			InitializeFood(&foodHeade, 3);
		}
		SnakeDie(snakeheade, &user);
		EndBatchDraw();
	}
	system("pause");
	closegraph();			// 关闭绘图窗口
	return 0;
}