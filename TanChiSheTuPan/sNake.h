#pragma once
#include"Tool.h"
#include"Food.h"
#include<windows.h>
typedef DWORD COLOREF;
// 坐标
typedef struct Snake
{
	XyColour snakeXYC;
	int direction;		// 方向
	Snake* front;		// 前
	Snake* next;		// 后
}snake;

// 蛇的头结点
void SnakeHead(Snake** snakeHead)
{
	(*snakeHead) = (Snake*)malloc(sizeof(Snake));
	// 坐标
	(*snakeHead)->snakeXYC.x = 600;
	(*snakeHead)->snakeXYC.y = 0;
	// 初始方向
	(*snakeHead)->direction = right_t;
	// 初始颜色
	(*snakeHead)->snakeXYC.colour = WHITE;
	// 前结点
	(*snakeHead)->front = (*snakeHead);
	// 后结点
	(*snakeHead)->next = (*snakeHead);
}

// 初始化蛇的数据：尾插法，双向循环链表
void InitializeSnake(Snake* heade_t, int length)
{
	Snake* p,*q = heade_t->front;
	for (size_t i = 0; i < length; i++)
	{
		p = (Snake*)malloc(sizeof(Snake));
		// x 赋值,等于上结点减 50
		p->snakeXYC.x = q->snakeXYC.x - 50;
		// 赋值 y ，等于头结点
		p->snakeXYC.y = q->snakeXYC.y;
		// 方向等于上结点方向
		p->direction = q->direction;
		// 颜色
		p->snakeXYC.colour = BGR(RGB(rand() & 0xFF, rand() & 0xFF, rand() & 0xFF));
		// 尾结点：尾结点的 next 指向头结点
		p->next = heade_t;
		// 尾结点：尾结点的 frint 指向上一个结点 q
		p->front = q;
		// 倒数第二结点：尾结点的上一结点 q 指向尾结点 p
		q->next = p;
		// 头结点：头结点的 front 指向尾结点 p
		heade_t->front = p;
		// 标示结点 q 后移 p 的位置
		q = p;
	}
}
// 输出
void Output(Snake* snake_t)
{
	Snake* q = snake_t->next;
	while (q->snakeXYC.colour != WHITE)
	{
		printf("q.x = %d\n", q->snakeXYC.x);
		q = q->next;
	}
	printf("q->next = %p\n", q);
}

// 画蛇
void DrawSnake(Snake* heade)
{
	while (heade->next->snakeXYC.colour != WHITE)
	{
		setfillcolor(heade->snakeXYC.colour);
		fillrectangle(heade->snakeXYC.x, heade->snakeXYC.y, heade->snakeXYC.x + 50, heade->snakeXYC.y + 50);
		heade = heade->next;
	}
	printf("\n");
}


// 蛇行走
void SnakeCraw(Snake* snake_t, XyColour* backeGround_t)
{
	Snake* q = snake_t->front;
	// 蛇身的移动
	while (q->snakeXYC.colour != WHITE)
	{
		Delay(1000);
		q->snakeXYC.x = q->front->snakeXYC.x;
		q->snakeXYC.y = q->front->snakeXYC.y;
		q->direction = q->front->direction;
		q = q->front;
	}
	// 蛇头
	switch (snake_t->direction)
	{
	case up:
		if (snake_t->snakeXYC.y > 0)
		{
			snake_t->snakeXYC.y -= 50;
			backeGround_t->y -= 60;
		}
		else
		{
			backeGround_t->y = backeGround_t->y;
			snake_t->snakeXYC.y = snake_t->snakeXYC.y;
		}
		break;
	case down:
		if (snake_t->snakeXYC.y < 1000)
		{
			snake_t->snakeXYC.y += 50;
			backeGround_t->y += 60;
		}
		else
		{
			snake_t->snakeXYC.y = snake_t->snakeXYC.y;
			backeGround_t->y = backeGround_t->y;
		}
		break;
	case left_t:
		if (snake_t->snakeXYC.x >= 200)
		{
			snake_t->snakeXYC.x -= 50;
			backeGround_t->x -= 60;
		}
		else
		{
			snake_t->snakeXYC.x = snake_t->snakeXYC.x;
			backeGround_t->x = backeGround_t->x;
		}
		break;
	case right_t:
		if (snake_t->snakeXYC.x <= 1450)
		{
			snake_t->snakeXYC.x += 50;
			backeGround_t->x += 60;
		}
		else
		{
			snake_t->snakeXYC.x = snake_t->snakeXYC.x;
			backeGround_t->x = backeGround_t->x;
		}
		break;
	}
}

